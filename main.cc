#include <iostream>

//#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
 // TypeName(const TypeName&);   \
 // void operator=(const TypeName&) 

class TestClass {
 public:
  TestClass() : value(0), ptr(new int) {}
  ~TestClass() { delete ptr; };
 
// DISALLOW_COPY_AND_ASSIGN(TestClass);
  int value;
  int *ptr;
};

void Add(TestClass TmpClass)
{
    TmpClass.value++;
    (*(TmpClass.ptr))++;
    std::cout << "# The values here are: " << TmpClass.value << " and " << (*TmpClass.ptr) << std::endl;
}

int main(int argc, char** argv)
{
    TestClass MyClass;
    MyClass.value = 2;
    *(MyClass.ptr) = 2;

    Add(MyClass);

    return 0;
}
